import math
import jax
import jax.lax as lax
import jax.numpy as jnp
import jax.random as jrandom
import equinox as eqx
import pandas as pd
import tqdm
import optax
import numpy as np
from functools import partial
from jax import jit
from jax import grad
from jax.scipy.optimize import minimize

import jax.nn as jnn

PRINT_SHAPES = True 

def dataloader(arrays, batch_size, *, key):
    dataset_size = arrays[0].shape[0]
    assert all(array.shape[0] == dataset_size for array in arrays)
    indices = jnp.arange(dataset_size)
    while True:
        perm = jrandom.permutation(key, indices)
        (key,) = jrandom.split(key, 1)
        start = 0
        end = batch_size
        while end < dataset_size:
            batch_perm = perm[start:end]
            yield tuple(array[batch_perm] for array in arrays)
            start = end
            end = start + batch_size

print('LOADING DATAS..')
import numpy as np
file = jnp.load('hm_detector_truth.npz')
hm_detector = file['detector']
hm_truth = file['truth']

hm_detector = jnp.array(hm_detector[0:10000])
hm_truth = jnp.array(hm_truth[0:10000])

print(type(hm_detector), type(hm_truth))
print(hm_detector.shape, hm_truth.shape)

print('DEFINING MODEL..')
class CNN(eqx.Module):
    #specify module's attributes
    layers: list
    activation: callable
    bias: jnp.ndarray

    #initializiting them
    def __init__(self, key):
        key1, key2, key3, key4, key5, key5, key6, key7, key8, key9, key10 = jrandom.split(jrandom.PRNGKey(467), 11)
        self.layers = [eqx.nn.Conv2d(1,8,5,stride=2, key=key1),
        eqx.nn.Conv2d(8,16,5,stride=2, key=key2),
        eqx.nn.Conv2d(16,32,5,stride=2, key=key3),
        eqx.nn.Linear(32*13*13, 512, key=key4),
        eqx.nn.Linear(512, 128, key=key5),
        eqx.nn.Linear(128,512, key=key6),
        eqx.nn.Linear(512, 32*13*13, key=key7),
        eqx.nn.ConvTranspose(num_spatial_dims =2,in_channels =32,out_channels =16,kernel_size = 5,stride=2,padding=0, output_padding=0, key=key8),
        eqx.nn.ConvTranspose(num_spatial_dims=2,in_channels=16,out_channels =8,kernel_size = 5,stride=2,padding=0, output_padding=1, key=key9),
        eqx.nn.ConvTranspose(num_spatial_dims=2,in_channels=8,out_channels =1,kernel_size = 5,stride=2,padding=0, output_padding=1, key=key10)]
        
        self.activation = jnn.relu
        self.bias = jnp.ones(2)

    def __call__(self,x):
        print('input: ', x.shape)
        #x = jnp.reshape(x, (1, x.shape[0], x.shape[1]))
        print('input: ', x.shape)
        
        x = self.layers[0](x)
	
       # print('conv1:', x.shape)            
        x = self.layers[1](x)
        x = self.activation(x) 
       # print('conv2:', x.shape)
        x = self.layers[2](x)
        x = self.activation(x)
       
        x = self.layers[7](x)
        x = self.activation(x)
       # print('convT:', x.shape)
        x = self.layers[8](x)
        x = self.activation(x)
        
       # print('convT:', x.shape)
        x = self.layers[9](x)
        x = self.activation(x) 
        
       # print('out:', x.shape)
        return x

def main(
    batch_size = 1,
    steps = 400,
    learning_rate = 0.001,
    seed = 5438,
):
    print('main')
   # data_key, loader_key, model_key = jrandom.split(jrandom.PRNGKey(seed), 3)
    xs = hm_detector[0:9000]
    ys = hm_truth [0:9000]
    xv = hm_detector[9000:10000]
    yv = hm_truth[9000:10000]

    print(xs.shape, ys.shape)    
    xs = jnp.reshape(xs, (xs.shape[0], 1, xs.shape[1], xs.shape[2]))
    print(xs.shape)
    iter_data = dataloader((xs, ys), batch_size, key = jrandom.PRNGKey(68743))
    model = CNN(jrandom.PRNGKey(234))
    
    def linear(params, X):
        a, b = params
        return a * X + b 
    
    def prova(params, X, y):
        return jnp.mean((linear(params, X) -y)**2)
    '''
    def m_predicted(model, x, y):
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size, 128, 128))

        indexess = jnp.argwhere(jnp.where(pred_y>0.5, pred_y, 0))
       
        X = jnp.array(indexess[:,0])
        Y = jnp.array(indexess[:,1])
        loss = jnp.mean((linear(params, X) - y)**2)
        results = minimize(loss, jnp.array([1.0, 2.0]), args = (X,y), method = 'BFGS')
        m_predicted = results[0][0]
        
        return m_predicted
        
    def m_truth(y):
        y = jnp.reshape(y, (batch_size, 128, 128))
        for i in range(batch_size):
            img = y[i]   
            print(img.shape) 
            indexess = jnp.argwhere(jnp.where(img>0.5, img, 0))
            X = jnp.array(indexess[:,0])
            Y = jnp.array(indexess[:,1])
            print(X.shape, Y.shape)
            loss = jnp.mean((linear(params, X) - img)**2)
            results = minimize(loss, jnp.array([1.0, 2.0]), args = (X, img), method = 'BFGS')
            result = results[0][0]
            print(result)
        return result
    

         
    @eqx.filter_value_and_grad
    def compute_loss(model, x, y):
        print(x.shape)
        print(y.shape)
        
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size,128,128))

        print(pred_y.shape)
              
        x = jnp.ravel(x)
        y = jnp.ravel(y)
        pred_y = jnp.ravel(pred_y)
        #print('shapes per loss')
        print(x.shape, y.shape, pred_y.shape)
       
        loss = (pred_y - y)**2
        loss_nozeros = jnp.where(x!=0, loss, 0)
        
        mse = loss_nozeros.sum() / jnp.count_nonzero(loss_nozeros)
       
        return mse
    
    @eqx.filter_value_and_grad
    def compute_loss(model, x, y):
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size,128,128))
        #vector_m_diff = jnp.empty(batch_size)
        #vector_m_pred = jnp.empty(batch_size)  
        for i in range(batch_size): 
            img_tru = y[i]
            img_pred = pred_y[i]
            print(img_tru.shape, img_pred.shape, "img nel for")
            truth_pass = jnp.where(img_tru>0.5, img_tru, 0)
            print('prova3', truth_pass.shape)
            
            indexess_t = jnp.argwhere(truth_pass, size = jnp.count_nonzero(truth_pass))
            print('prova4', indexess_t.shape)
            pred_pass = jnp.where(img_pred>0.5, img_pred, 0)
            print('prova5')
            indexess_p = jnp.argwhere(pred_pass, size = len(pred_pass))
            print('prova6')
            Xt, Yt = jnp.array(indexess_t[:,0]), jnp.array(indexess_t[:,1])
            Xp, Yp = jnp.array(indexess_p[:,0]), jnp.array(indexess_p[:,1])
            print(Xt.shape, Yt.shape, Xp.shape, Yp.shape)
            results_t = minimize(prova, jnp.array([1.0, 2.0]), args = (Xt, Yt), method = 'BFGS')
            results_p = minimize(prova, jnp.array([1.0, 2.0]), args = (Xp, Yp), method = 'BFGS')
            print('----')
            print(results_t)
            mt = results_t.x[0]
            mp = results_p.x[0]
            print('m', mt.shape, mp.shape)
           
          
            loss_m = (mt - mp)**2
            #loss_q = (qm - qt)**2
            print('diff m', loss_m)
            vector_m_diff = jnp.append(vector_m_diff, loss_m)
            #qmse = loss_q.sum()      
        print(vector_m_diff, "vector m diff")
        m_mse = vector_m_diff.sum()
        return m_mse #, qms
    '''
    @eqx.filter_value_and_grad
    def compute_loss(model,x,y,vector_m_truth, vector_m_pred):
        mt = vector_m_truth.ravel()
        mp = vector_m_pred.ravel()
       
        diff = (mt - mp)**2
        return diff.sum()


    @eqx.filter_jit
    def make_step(model, x,y,vector_m_truth, vector_m_pred, opt_state):
        print('======')
        loss, grads = compute_loss(model, x, y, vector_m_truth, vector_m_pred)
        updates, opt_state = optim.update(grads, opt_state)
        model = eqx.apply_updates(model, updates)
        return loss, model, opt_state

    optim = optax.adam(learning_rate)
    opt_state = optim.init(eqx.filter(model, eqx.is_array))

    for step, (x, y) in zip(range(steps), iter_data):
        #y = jnp.reshape(y, (batch_size, 128, 128)) vettori qui
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size, 128, 128))
        vector_m_truth = jnp.empty(batch_size)
        vector_m_pred = jnp.empty(batch_size) 
        for i in range(batch_size):
              img_tru = y[i]   
              img_pred = pred_y[i]
              print(img_tru.shape, img_pred.shape, " test") 
              indexesst = jnp.argwhere(jnp.where(img_tru>0.5, img_tru, 0))
              indexessp = jnp.argwhere(jnp.where(img_pred>0.5, img_pred, 0))
              
              Xt = jnp.array(indexesst[:,0])
              Yt = jnp.array(indexesst[:,1])
              Xp = jnp.array(indexessp[:,0])
              Yp = jnp.array(indexessp[:,1])
              print(Xt, Yt, Xp, Yp)
              print(Xt.shape, Yt.shape)
              print(Xp.shape, Yp.shape)
              resultst = minimize(prova, jnp.array([1.0, 2.0]), args = (Xt, Yt), method = 'BFGS')
              mt = resultst[0][0]
              #jnp.append(vector_m_truth, mt)
              vector_m_truth = vector_m_truth.at[i].set(mt)

              resultsp = minimize(prova, jnp.array([1.0, 2.0]), args = (Xp, Yp), method = 'BFGS')
              mp = resultsp[0][0]
              #jnp.append(vector_m_pred, mp)
              vector_m_pred = vector_m_pred.at[i].set(mp)
              print('prova2')
              print(mt, mp)
              #result = results[0][0]  #jnp.array dim batch]
        


        print(vector_m_truth, vector_m_pred, 'vettori m')


        print('XSHHHAPE', x.shape)
        #x = jnp.reshape(x, (1, 128, 128))
        #print('NEWSHHHHHAPE', x.shape)
       
        loss, model, opt_state = make_step(model,x,y,vector_m_truth, vector_m_pred,  opt_state)
        loss = loss.item()
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size, 128, 128))
        print(y[0][0][0], pred_y[0][0][0])
        
        #print(y.shape, pred_y.shape)
        print(f"step={step}, loss={loss}")
        
    print('END TRAINING')
    print(xs.shape)
    
    xv = jnp.reshape(xv, (xv.shape[0],1, xv.shape[1], xv.shape[2]))     
    pred_yv = jax.vmap(model)(xv)
    print(pred_yv.shape)
    jnp.savez('predicted_equinox', pred_yv, xv, yv, pred_yv = pred_yv, xv = xv, yv=yv)
    print('CIAAOOO')

main()
