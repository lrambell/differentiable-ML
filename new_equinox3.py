import math
import jax
import jax.lax as lax
import jax.numpy as jnp
import jax.random as jrandom
import equinox as eqx
import pandas as pd
import tqdm
import optax
import numpy as np
from functools import partial
from jax import jit
from jax import grad
from jax.scipy.optimize import minimize

import numpy as np
import matplotlib.pyplot as plt

import jax.nn as jnn

PRINT_SHAPES = True 

def dataloader(arrays, batch_size, *, key):
    dataset_size = arrays[0].shape[0]
    assert all(array.shape[0] == dataset_size for array in arrays)
    indices = jnp.arange(dataset_size)
    while True:
        perm = jrandom.permutation(key, indices)
        (key,) = jrandom.split(key, 1)
        start = 0
        end = batch_size
        while end < dataset_size:
            batch_perm = perm[start:end]
            yield tuple(array[batch_perm] for array in arrays)
            start = end
            end = start + batch_size

print('LOADING DATAS..')
import numpy as np
file = jnp.load('hm_detector_truth.npz')
hm_detector = file['detector']
hm_truth = file['truth']

hm_detector = jnp.array(hm_detector[0:10000])
hm_truth = jnp.array(hm_truth[0:10000])

print(type(hm_detector), type(hm_truth))
print(hm_detector.shape, hm_truth.shape)

print('DEFINING MODEL..')
class CNN(eqx.Module):
    #specify module's attributes
    layers: list
    activation: callable
    bias: jnp.ndarray

    #initializiting them
    def __init__(self, key):
        key1, key2, key3, key4, key5, key5, key6, key7, key8, key9, key10 = jrandom.split(jrandom.PRNGKey(467), 11)
        self.layers = [eqx.nn.Conv2d(1,8,5,stride=2, key=key1),
        eqx.nn.Conv2d(8,16,5,stride=2, key=key2),
        eqx.nn.Conv2d(16,32,5,stride=2, key=key3),
        eqx.nn.Linear(32*13*13, 512, key=key4),
        eqx.nn.Linear(512, 128, key=key5),
        eqx.nn.Linear(128,512, key=key6),
        eqx.nn.Linear(512, 32*13*13, key=key7),
        eqx.nn.ConvTranspose(num_spatial_dims =2,in_channels =32,out_channels =16,kernel_size = 5,stride=2,padding=0, output_padding=0, key=key8),
        eqx.nn.ConvTranspose(num_spatial_dims=2,in_channels=16,out_channels =8,kernel_size = 5,stride=2,padding=0, output_padding=1, key=key9),
        eqx.nn.ConvTranspose(num_spatial_dims=2,in_channels=8,out_channels =1,kernel_size = 5,stride=2,padding=0, output_padding=1, key=key10)]
        
        self.activation = jnn.relu
        self.bias = jnp.ones(2)

    def __call__(self,x):
        print('input: ', x.shape)
        #x = jnp.reshape(x, (1, x.shape[0], x.shape[1]))
        print('input: ', x.shape)
        
        x = self.layers[0](x)
	
       # print('conv1:', x.shape)            
        x = self.layers[1](x)
        x = self.activation(x) 
       # print('conv2:', x.shape)
        x = self.layers[2](x)
        x = self.activation(x)
       
        x = self.layers[7](x)
        x = self.activation(x)
       # print('convT:', x.shape)
        x = self.layers[8](x)
        x = self.activation(x)
        
       # print('convT:', x.shape)
        x = self.layers[9](x)
        x = self.activation(x) 
        
       # print('out:', x.shape)
        return x

def main(
    batch_size = 32,
    steps = 200,
    learning_rate = 0.001,
    seed = 5438,
    alpha = 1,
    beta = 1, 
):
    print('main')
   # data_key, loader_key, model_key = jrandom.split(jrandom.PRNGKey(seed), 3)
    xs = hm_detector[0:9000]
    ys = hm_truth [0:9000]
    xv = hm_detector[9000:10000]
    yv = hm_truth[9000:10000]

    print(xs.shape, ys.shape)    
    xs = jnp.reshape(xs, (xs.shape[0], 1, xs.shape[1], xs.shape[2]))
    print(xs.shape)
    iter_data = dataloader((xs, ys), batch_size, key = jrandom.PRNGKey(68743))
    model = CNN(jrandom.PRNGKey(234))
    
    def linear(params, X):
        a, b = params
        return a * X + b 
    
    def prova(params, X, y):
        return jnp.mean((linear(params, X) -y)**2)
       
    @eqx.filter_value_and_grad
    def compute_loss_mse(model, x,y):
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size, 128, 128))
        x = jnp.ravel(x)
        y = jnp.ravel(y)
        pred_y = jnp.ravel(pred_y)
        loss_mse = (pred_y - y)**2
        loss_mse_nozeros = jnp.where(x!=0, loss_mse, 0)
        return loss_mse_nozeros.sum()/jnp.count_nonzero(loss_mse_nozeros)

    @eqx.filter_value_and_grad
    def compute_loss_fit(vector_m_truth, vector_m_pred):
        mt = vector_m_truth.ravel()
        mp = vector_m_pred.ravel()
        diff = (mt - mp)**2
        return diff.sum()

    @eqx.filter_value_and_grad
    def compute_loss(model,x,y,vector_m_truth, vector_m_pred):
        loss_mse = compute_loss_mse(model,x,y)
        loss_fit = compute_loss_fit(vector_m_truth, vector_m_pred)
        return alpha*loss_mse + beta*loss_fit        
   

    @eqx.filter_jit
    def make_step(model, x,y,vector_m_truth, vector_m_pred, opt_state):
        print('======')
        loss, grads = compute_loss(model, x, y, vector_m_truth, vector_m_pred)
        updates, opt_state = optim.update(grads, opt_state)
        model = eqx.apply_updates(model, updates)
        mse = compute_loss_mse(model, x, y)
        fit = compute_loss_fit(vector_m_truth, vector_m_pred)
        return loss, model, opt_state, mse, fit

    optim = optax.adam(learning_rate)
    opt_state = optim.init(eqx.filter(model, eqx.is_array))
    loss_vector = jnp.empty(steps)
    mse_vector = jnp.empty(steps)
    fit_vector = jnp.empty(steps)
    for step, (x, y) in zip(range(steps), iter_data):
        #y = jnp.reshape(y, (batch_size, 128, 128)) vettori qui
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size, 128, 128))
        vector_m_truth = jnp.empty(batch_size)
        vector_m_pred = jnp.empty(batch_size) 
        vector_em_pred = jnp.empty(batch_size)
        for i in range(batch_size):
              img_tru = y[i]   
              img_pred = pred_y[i]
              print(img_tru.shape, img_pred.shape, " test")
                          
              indexesst = jnp.argwhere(jnp.where(img_tru>0.5, img_tru, 0))
              ywhere_p = jnp.where(img_pred>0.5, img_pred, 0)
              indexessp = jnp.argwhere(jnp.where(img_tru>0.5, ywhere_p, 0))
              #ywhere_p = jnp.where(x==0, img_pred, 0)
              #indexessp = jnp.argwhere(jnp.where(ywhere_p>0.5, ywhere_p, 0))
              #indexessp = jnp.argwhere(jnp.where(img_pred>0.5, img_pred, 0))
              Xt = jnp.array(indexesst[:,0])
              Yt = jnp.array(indexesst[:,1])
              Xp = jnp.array(indexessp[:,0])
              Yp = jnp.array(indexessp[:,1])
              print(Xt, Yt, Xp, Yp)
              print(Xt.shape, Yt.shape)
              print(Xp.shape, Yp.shape)
              resultst = minimize(prova, jnp.array([1.0, 2.0]), args = (Xt, Yt), method = 'BFGS')
              mt = resultst[0][0]
              vector_m_truth = vector_m_truth.at[i].set(mt)

              resultsp = minimize(prova, jnp.array([1.0, 2.0]), args = (Xp, Yp), method = 'BFGS')
              mp = resultsp[0][0]
              ep = jnp.sqrt(jnp.diag(resultsp.hess_inv))
              
              vector_m_pred = vector_m_pred.at[i].set(mp)
              vector_em_pred = vector_em_pred.at[i].set(ep[0])
              
              print('prova2')
              print(f"mt: {mt}, mp: {mp}, ep: {ep[0]}")
                    


        print(vector_m_truth, vector_m_pred, 'vettori m')


        print('XSHHHAPE', x.shape)
        #x = jnp.reshape(x, (1, 128, 128))
        #print('NEWSHHHHHAPE', x.shape)
       
        loss, model, opt_state, mse, fit = make_step(model,x,y,vector_m_truth, vector_m_pred,  opt_state)
        loss = loss.item()
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size, 128, 128))
        print(y[0][0][0], pred_y[0][0][0])
        loss_vector = loss_vector.at[step].set(loss)
        mse_vector = mse_vector.at[step].set(mse)
        fit_vector = mse_vector.at[step].set(fit)
        
        #print(y.shape, pred_y.shape)
        print(f"step={step}, loss={loss}")
        
    print('END TRAINING')
    plt.title('Loss study')
    plt.plot(jnp.arange(steps), loss_vector, 'k.', label = 'total loss')
    plt.plot(jnp.arange(steps), mse_vector, 'b.', label = 'mse loss')
    plt.plot(jnp.arange(steps), fit_vector, 'm.', label = 'fit loss')
    plt.legend()
    plt.show()
    


    print(xs.shape)
    
    xv = jnp.reshape(xv, (xv.shape[0],1, xv.shape[1], xv.shape[2]))     
    pred_yv = jax.vmap(model)(xv)
    print(pred_yv.shape)
    jnp.savez('predicted_equinox', pred_yv, xv, yv, pred_yv = pred_yv, xv = xv, yv=yv)
    print('CIAAOOO')

main()
