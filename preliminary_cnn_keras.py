
import numpy as np
print('1')
import tensorflow as tf
print('2')

import matplotlib.pyplot as plt
print('----------')
events = np.load('events_small.npz')
targets = np.load('target_small.npz')
data = events['data']
target = targets['target']

print(data.shape, target.shape)

#plt.imshow(data[0], aspect='auto', cmap = 'binary')

#plt.imshow(target[0], aspect='auto', cmap = 'binary')

input = tf.keras.layers.Input(shape = (3000,16,1))
print(input.shape)
x = tf.keras.layers.Conv2D(2, (16, 6), padding='valid', activation = "relu", name = "conv1")(input)
print(x.shape)
x = tf.keras.layers.MaxPooling2D((2, 1), name = "pool1")(x)
print(x.shape)
x = tf.keras.layers.Conv2D(4, (2, 6), padding='valid', activation = "relu", name = "conv2")(x)
print(x.shape)
x = tf.keras.layers.MaxPooling2D((2, 1), name = "pool3")(x)
print(x.shape)
x = tf.keras.layers.Conv2D(8, (8, 6), padding='valid', activation = "relu", name = "conv4")(x)
print(x.shape)
x = tf.keras.layers.MaxPooling2D((2, 1), name = "pool4")(x)
print(x.shape)
x = tf.keras.layers.Conv2DTranspose(8, (16,8), strides = (2,2), padding = 'valid', activation = 'relu')(x)
print(x.shape)
x = tf.keras.layers.Conv2DTranspose(4, (2,2), strides = (2,2), padding = 'valid', activation = 'relu')(x)
print(x.shape)
x = tf.keras.layers.Conv2DTranspose(2, (2,2), strides = (2,1), padding = 'valid', activation = 'relu')(x)
print(x.shape)
output = tf.keras.layers.Conv2D(1, (9, 2), padding='valid', activation = "relu")(x)
print(output.shape)

data = np.reshape(data, (data.shape[0], 3000, 16, 1))
target = np.reshape(target, (target.shape[0], 3000, 16, 1))

data_training = data[0:1000]
target_training = target[0:1000]

data_test = data[0:1000]
target_test = target[0:1000]

print(data_training.shape, data_test.shape)

def custom_loss_function(y_true, y_pred):
   squared_difference = tf.square((y_true-1) - y_pred)
   squared_difference = 1*squared_difference[y_true > 0]
   return tf.reduce_mean(squared_difference, axis=-1)

model = tf.keras.Model(input, output)
model.summary()
model.compile(loss=custom_loss_function, optimizer='rmsprop', metrics=['mae'])

history = model.fit(data_training, target_training, validation_data=(data_test, target_test), epochs=100,batch_size=128)
plt.plot(history.history['loss'], label = 'loss')
plt.plot(history.history['val_loss'], label = 'val_loss')
plt.legend()
plt.savefig('loss_keras.png')


#model.save('model_cnn_keras.h5')
#model.save('./weights_model_cnn_keras/')

data_predict = data_test
target_predict = target_test
predicted_image = model.predict(data_predict)
predicted_image = predicted_image.reshape(data_predict.shape[0],data.shape[1],data.shape[2])
target_image = target_predict.reshape(data_predict.shape[0],data.shape[1],data.shape[2])


fig, ax = plt.subplots(figsize=(20, 20))
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)

predicted_image = predicted_image.flatten()

target_image = target_image.flatten()

predicted_image_signal = predicted_image[target_image == 2]
predicted_image_bkg    = predicted_image[target_image == 1]

plt.figure(figsize=(8,6))
plt.hist(predicted_image_signal,range=[-1,2.2], bins=200, alpha=0.5, label="signal")
plt.hist(predicted_image_bkg,   range=[-1,2.2], bins=200, alpha=0.5, label="bkg")
plt.xlim([-0.2, 2.2])
plt.yscale('log')
plt.xlabel("Output", size=14)
plt.ylabel("Count", size=14)
plt.legend(loc='upper right')
plt.savefig("predictions_CNN_keras.png")



