import numpy as np
from tqdm import tqdm
import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pandas as pd
from torch.utils.data.dataset import TensorDataset
from torch.utils.data import DataLoader
import seaborn as sns
from torch import optim

print('LOADING DATASET..')
dataset = np.load('Tracks_nn.npz')
events = dataset['events']

# GLOBAL GEOMETRY PARAMETERS
HITMAP_SIZE_X = 128
HITMAP_SIZE_Y = 128
MIN_X=-100.
MIN_Y=-100.
MAX_X= 100.
MAX_Y= 100.

# GLOBAL SAMPLE PARAMETERS
LEARNING_RATE = 0.001
N_EPOCHS = 10
SAMPLE_SIZE = 10000
TRAIN_SIZE  = int(0.9*SAMPLE_SIZE)
BATCH_SIZE = 100

# GENERAL SETUP
PRINT_SHAPES = True

# FUNCTION TO TRANSLATE VECTORS OF POSITIONS/LABELS INTO
def getHitmaps(x,y,labels,option_012=True) :
  # Create detector and truth hitmaps
  hm_detector = np.zeros((HITMAP_SIZE_Y,HITMAP_SIZE_X))
  hm_truth    = np.zeros((HITMAP_SIZE_Y,HITMAP_SIZE_X))
  
  # Fill detector and truth hitmaps
  for i in range(x.size):
    if x[i]<MIN_X or x[i]>=MAX_X or y[i]<MIN_Y or y[i]>=MAX_Y : continue  # checks there are no hits outside the desired window
    binx = math.floor(HITMAP_SIZE_X*(x[i]-MIN_X)/(MAX_X-MIN_X))
    biny = math.floor(HITMAP_SIZE_Y*(y[i]-MIN_Y)/(MAX_Y-MIN_Y))
    hm_detector[biny][binx] = 1
    if(option_012) :
      hm_truth[biny][binx] = labels[i]+1
    else :
      if labels[i]>0 : hm_truth[biny][binx] = 1
  # Return hitmaps
  return hm_detector, hm_truth


# READ EVENTS
coord_x = events[:,:,0] # get all x values for each event -> already a numpy.array
coord_y = events[:,:,1] # get all y values for each event -> already a numpy.array
labels  = events[:,:,2] # get all labels for each event -> already a numpy.array
if PRINT_SHAPES :
  print(coord_x.shape)
  print(coord_y.shape)
  print(labels.shape)

# CREATE DATA FRAME
DETECTOR = []
TRUTH    = []
# Loop on events, create hitmaps and store them
for i in tqdm(range(SAMPLE_SIZE)): # tqdm(range(len(coord_x))):
  hm_detector,hm_truth = getHitmaps(coord_x[i],coord_y[i],labels[i],False)
  DETECTOR.append(hm_detector)
  TRUTH.append(hm_truth)
d = {'detector': DETECTOR, 'truth': TRUTH}
df = pd.DataFrame(data = d)
print(df)

# CREATE DATA LOADER
detector_full = torch.FloatTensor(df.detector)
print('DETECTOR FULL')
print(detector_full)
truth_full    = torch.FloatTensor(df.truth)
detector_train = detector_full[:TRAIN_SIZE]
truth_train    = truth_full[:TRAIN_SIZE]
detector_valid = detector_full[TRAIN_SIZE:]
truth_valid    = truth_full[TRAIN_SIZE:]
train_ds = TensorDataset(detector_train,truth_train)
valid_ds = TensorDataset(detector_valid,truth_valid)
train_dataloader = DataLoader(train_ds,batch_size=BATCH_SIZE,shuffle=True)
valid_dataloader = DataLoader(valid_ds,batch_size=BATCH_SIZE,shuffle=False)

class CNN_LUCREZIA(nn.Module):

    def __init__(self):
        super(CNN, self).__init__()

        self.conv1 = nn.Conv2d(1, 60, 5)
        self.conv2 = nn.Conv2d(60, 60, 10)
        self.conv3 = nn.Conv2d(60, 30, 10)
        self.conv4 = nn.Conv2d(30, 10, 15)
        self.linear1 = nn.Linear(150*10, 5)
        #self.linear2 = nn.Linear(10, 30)
        self.convt1 = nn.ConvTranspose2d(10, 30, 5)
        self.convt2 = nn.ConvTranspose2d(30, 60 ,10 )
        self.convt3 = nn.ConvTranspose2d(60, 90 , 10)
        self.convt4 = nn.ConvTranspose2d(90, 1 , 15 )
        self.activation1 = nn.ReLU()

    def forward(self, x):
        if PRINT_SHAPES : print('input: ', x.shape)
        x=x.unsqueeze(1) # uses the batch size as shape of first dimension
        if PRINT_SHAPES : print('input: ', x.shape)
        x = self.conv1(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('conv1: ', x.shape)
        x = self.conv2(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('conv2: ',x.shape)
        x = self.conv3(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('conv3: ',x.shape)
        x = self.conv4(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('conv4: ',x.shape)
        x = self.convt1(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('convt1:',x.shape)
        x = self.convt2(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('convt2:',x.shape)
        x = self.convt3(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('convt3:',x.shape)
        x = self.convt4(x)
        out = self.activation1(x)
        if PRINT_SHAPES : print('output:',x.shape)

        return out

class CNN(nn.Module):

    def __init__(self):
        super(CNN, self).__init__()

        self.conv1 = nn.Conv2d(1, 8, 5, stride=2)
        self.conv2 = nn.Conv2d(8, 16, 5, stride=2)
        self.conv3 = nn.Conv2d(16, 32, 5, stride=2)
        self.linear1 = nn.Linear(32*13*13, 512)
        self.linear2 = nn.Linear(512, 128)
        self.linear2t = nn.Linear(128, 512)
        self.linear1t = nn.Linear(512, 32*13*13)
        self.convt1 = nn.ConvTranspose2d(32, 16, 5, stride=2, padding=0, output_padding=0)
        self.convt2 = nn.ConvTranspose2d(16, 8, 5, stride=2, padding=0, output_padding=1)
        self.convt3 = nn.ConvTranspose2d(8, 1, 5, stride=2, padding=0, output_padding=1)
        self.activation1 = nn.ReLU()

    def forward(self, x):
        if PRINT_SHAPES : print('input: ', x.shape)
        x=x.unsqueeze(1) # uses the batch size as shape of first dimension
        if PRINT_SHAPES : print('input: ', x.shape)
        x = self.conv1(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('conv1: ', x.shape)
        x = self.conv2(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('conv2: ',x.shape)
        x = self.conv3(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('conv3: ',x.shape)
        x = torch.flatten(x, 1)
        if PRINT_SHAPES : print('flatten: ',x.shape)
        x = self.linear1(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('linear1:',x.shape)
        x = self.linear2(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('linear2:',x.shape)
        x = self.linear2t(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('linear2t:',x.shape)
        x = self.linear1t(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('linear1t:',x.shape)
        x = torch.reshape(x, [BATCH_SIZE,32,13,13])
        # x = torch.unflatten(x, 1, [32,11,11]) # NOT AVAILABLE IN MY CONDA PYTORCH VERSION
        if PRINT_SHAPES : print('unflatten: ',x.shape)
        x = self.convt1(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('convt1:',x.shape)
        x = self.convt2(x)
        x = self.activation1(x)
        if PRINT_SHAPES : print('convt2:',x.shape)
        x = self.convt3(x)
        out = self.activation1(x)
        if PRINT_SHAPES : print('output:',x.shape)

        return out

# CREATE CNN
cnn = CNN()
print(cnn)

loss_function = nn.MSELoss()
optimizer = optim.Adam(cnn.parameters(), lr = LEARNING_RATE)

# TRAINING LOOP
print('TRAINING!')
for epoch in tqdm(range(N_EPOCHS)):
  loss=0
  for detector,truth in train_dataloader:
    optimizer.zero_grad()
    output = cnn(detector)
    output = output.reshape(BATCH_SIZE,HITMAP_SIZE_Y,HITMAP_SIZE_X)

    mask = (torch.flatten(detector) != 0)

    #loss = loss_function(detector, output)
    loss = loss_function(torch.flatten(truth)[mask], torch.flatten(output)[mask])
                                                
    #loss = loss_function(torch.flatten(input_loss),torch.flatten(output))
    #loss = custom_loss_func(detector, output)
    #loss = custom_loss_func(torch.flatten(truth),torch.flatten(output))
    loss.backward()
    optimizer.step()
  print(loss)

# CREATE HITMAPS AND MAKE PLOTS FOR ONE EVENT
for detector,truth in valid_dataloader:
  output = cnn(detector)
  output = output.reshape(BATCH_SIZE,HITMAP_SIZE_Y,HITMAP_SIZE_X)
  mask = (detector[0].detach().numpy())
  fig_1,ax_1 = plt.subplots(nrows=2, ncols=2)
  plot_hm_detector1 = sns.heatmap(detector[0].detach().numpy(), square=True, cbar=False, ax=ax_1[0,0])
  plot_hm_truth1 = sns.heatmap(truth[0].detach().numpy(), square=True, cbar=False, ax=ax_1[0,1])
  plot_hm_output1 = sns.heatmap(output[0].detach().numpy(), square=True, cbar=False, ax=ax_1[1,0])
  plot_hm_output1 = sns.heatmap(output[0].detach().numpy()*mask, square=True, cbar=False, ax=ax_1[1,1])
  plt.show()
  break
