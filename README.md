# Differentiable ML for Pattern Recognition
Pattern recognition using JAX and EQUINOX libraries for adding physics informations in the training process.

## Setup


```bash
ssh -Y teogpu01
conda activate rambeluc_EQX
```

## Usage
Available scripts: 

- cnn_punti_retta.py for dataset simulation and download (as Tracks_nn.npz)
- cnn_pytorch.py pytorch-based script for pattern recognition 
- create_hm.py script for heatmaps creation starting by Tracks_nn.npz (return hm_detector_truth.npz file)
- new_equinox.py test for a jax-based CNN 
