import numpy as np
from tqdm import tqdm
import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pandas as pd
from torch.utils.data.dataset import TensorDataset
from torch.utils.data import DataLoader
import seaborn as sns
from torch import optim

print('LOADING DATASET..')
dataset = np.load('Tracks_nn.npz')
events = dataset['events']

# GLOBAL GEOMETRY PARAMETERS
HITMAP_SIZE_X = 128
HITMAP_SIZE_Y = 128
MIN_X=-100.
MIN_Y=-100.
MAX_X= 100.
MAX_Y= 100.

# GLOBAL SAMPLE PARAMETERS
LEARNING_RATE = 0.001
N_EPOCHS = 10
SAMPLE_SIZE = 10000
TRAIN_SIZE  = int(0.9*SAMPLE_SIZE)
BATCH_SIZE = 100

# GENERAL SETUP
PRINT_SHAPES = True

# FUNCTION TO TRANSLATE VECTORS OF POSITIONS/LABELS INTO
def getHitmaps(x,y,labels,option_012=True) :
  # Create detector and truth hitmaps
  hm_detector = np.zeros((HITMAP_SIZE_Y,HITMAP_SIZE_X))
  hm_truth    = np.zeros((HITMAP_SIZE_Y,HITMAP_SIZE_X))
  
  # Fill detector and truth hitmaps
  for i in range(x.size):
    if x[i]<MIN_X or x[i]>=MAX_X or y[i]<MIN_Y or y[i]>=MAX_Y : continue  # checks there are no hits outside the desired window
    binx = math.floor(HITMAP_SIZE_X*(x[i]-MIN_X)/(MAX_X-MIN_X))
    biny = math.floor(HITMAP_SIZE_Y*(y[i]-MIN_Y)/(MAX_Y-MIN_Y))
    hm_detector[biny][binx] = 1
    if(option_012) :
      hm_truth[biny][binx] = labels[i]+1
    else :
      if labels[i]>0 : hm_truth[biny][binx] = 1
  # Return hitmaps
  return hm_detector, hm_truth


# READ EVENTS
coord_x = events[:,:,0] # get all x values for each event -> already a numpy.array
coord_y = events[:,:,1] # get all y values for each event -> already a numpy.array
labels  = events[:,:,2] # get all labels for each event -> already a numpy.array
if PRINT_SHAPES :
  print(coord_x.shape)
  print(coord_y.shape)
  print(labels.shape)

# CREATE DATA FRAME
DETECTOR = []
TRUTH    = []
# Loop on events, create hitmaps and store them
for i in tqdm(range(SAMPLE_SIZE)): # tqdm(range(len(coord_x))):
  hm_detector,hm_truth = getHitmaps(coord_x[i],coord_y[i],labels[i],False)
  DETECTOR.append(hm_detector)
  TRUTH.append(hm_truth)
d = {'detector': DETECTOR, 'truth': TRUTH}
df = pd.DataFrame(data = d)
print(df)

np.savez('hm_detector_truth', DETECTOR, TRUTH, detector = DETECTOR, truth = TRUTH)

