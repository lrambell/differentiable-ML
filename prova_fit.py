import jax 
import jax.numpy as jnp 
from itertools import product 
import pandas as pd
from jax.config import config
import seaborn as sns
import matplotlib.pyplot as plt

config.update("jax_enable_x64", True)

ciao = jnp.load('predicted_equinox.npz')
xv = ciao['xv']
yv = ciao['yv']
pred_yv = ciao['pred_yv']

i = 20

yt = yv[i]
yp = pred_yv[i]
x = xv[i]

yt = jnp.reshape(yt, (128,128))
yp = jnp.reshape(yp, (128,128))
x = jnp.reshape(x, (128,128))

condition = yp > 0.5 
indexess = jnp.argwhere(condition)
#print(indexess)
import numpy as np
X = np.asarray(indexess[:,0])
Y = np.asarray(indexess[:,1])
print(X.shape, Y.shape)


from jaxfit import CurveFit

def linear(x,m,b):
  y = m * x + b
  return y
x = X 
y = Y

cf = CurveFit()
popt, pcov = cf.curve_fit(linear,x, y) 
print('fitted for predicted:', popt)

condition2 = yt > 0.8
indexess2 = jnp.argwhere(condition2)
#print(indexess)

X2 = np.asarray(indexess2[:,0])
Y2 = np.asarray(indexess2[:,1])
print(X2.shape, Y2.shape)
#print(type(X2), type(Y2))
#print(X2, Y2)

x = X2
y = Y2
cf = CurveFit()
popt2, pcov2 = cf.curve_fit(linear,x,y)
print('fitted for truth:   ',popt2)


yv = jnp.reshape(yv[0], (128,128))
pred_yv = jnp.reshape(pred_yv[0], (128,128))


mask = pred_yv>0.9

print(X[:10], Y[:10])
print(X2[:10], Y2[:10])
plt.plot(X, Y, 'r.', label='predicted')
plt.plot(X2,Y2, 'b.', label = 'truth')
plt.legend() 
plt.show()

fig1, ax1 = plt.subplots(1,3)
ax1[0].set(title = 'truth')
sns.heatmap(yv, square = True, ax=ax1[0])
ax1[1].set(title='predicted')
sns.heatmap(pred_yv, square=True, ax=ax1[1])
ax1[2].set(title = 'pred4fit')
sns.heatmap(pred_yv*mask, square = True, ax=ax1[2])

plt.show()

                 
