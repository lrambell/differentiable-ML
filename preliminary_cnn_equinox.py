import math
import jax
import jax.lax as lax
import jax.numpy as jnp
import jax.random as jrandom
import equinox as eqx
import pandas as pd
import tqdm
import optax
import numpy as np
from functools import partial
from jax import jit
from jax import grad
from jax.scipy.optimize import minimize

import numpy as np
import matplotlib.pyplot as plt

import jax.nn as jnn
from equinox import nn

def dataloader(arrays, batch_size, *, key):
    dataset_size = arrays[0].shape[0]
    assert all(array.shape[0] == dataset_size for array in arrays)
    indices = jnp.arange(dataset_size)
    while True:
        perm = jrandom.permutation(key, indices)
        (key,) = jrandom.split(key, 1)
        start = 0
        end = batch_size
        while end < dataset_size:
            batch_perm = perm[start:end]
            yield tuple(array[batch_perm] for array in arrays)
            start = end
            end = start + batch_size

print('Loading dataset')
truth = np.load('target_small.npz')
target = truth['target']
events = np.load('events_small.npz')
datas = events['data']
print(datas.shape, target.shape)

data = jnp.array(datas)
target = jnp.array(target)

class ConvNet(eqx.Module):
    activation: callable
    bias: jnp.ndarray
    layers: list
    def __init__(self, key):
        key0, key1, key2, key3, key4, key5, key5, key6 = jrandom.split(jrandom.PRNGKey(467), 8)
        self.layers = [nn.Conv2d(1, 2, (16, 6), padding=0, key = key0),
        nn.MaxPool2d((2, 1), (2,1)),         
        nn.Conv2d(2, 4, (2, 6), key=key1), 
        nn.MaxPool2d((2, 1), (2,1)), 
        nn.Conv2d(4, 8, (8, 6),  key=key2),
        nn.MaxPool2d((2, 1), (2,1)),
        nn.ConvTranspose2d(8, 8, (16,8), stride=2, key=key3),   
        nn.ConvTranspose2d(8, 4, (2,2), stride=2,  key=key4), 
        nn.ConvTranspose2d(4, 2, (2,2), stride=(2,1),  key=key5),           
        nn.Conv2d(2, 1, (9,2), key=key6)]  
        
        self.activation = jnn.relu
        self.bias = jnp.ones(2)

    def __call__(self, x):
        #print(x.shape)
        x = self.layers[0](x)
        x = self.activation(x)
        #print(x.shape)
        x = self.layers[1](x)
        #print(x.shape)
        x = self.layers[2](x)
        x = self.activation(x)
        #print(x.shape)
        x = self.layers[3](x)
        #print(x.shape)
        x = self.layers[4](x)
        x = self.activation(x)
        #print(x.shape)
        x = self.layers[5](x)
        #print(x.shape)
        x = self.layers[6](x)
        x = self.activation(x) 
        #print(x.shape)     
        x = self.layers[7](x)
        x = self.activation(x)
        #print(x.shape)
        x = self.layers[8](x)
        x = self.activation(x)
        #print(x.shape)
        x = self.layers[9](x)
        x = self.activation(x)
        #print(x.shape)
        
        return x

def main(
    batch_size = 2,
    steps = 100,
    learning_rate = 0.001,
    seed = 5438,
):

    xs = data[0:4]
    ys = target[0:4]
    xv = data[0:4]
    yv = target[0:4]

    xs = jnp.reshape(xs, (xs.shape[0], 1, 3000,16))
    iter_data = dataloader((xs, ys), batch_size, key= jrandom.PRNGKey(52769))
    model = ConvNet(jrandom.PRNGKey(234))
    print('Datas for training checked :)')

    @eqx.filter_value_and_grad
    def compute_loss(model, x,y):
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.ravel(jnp.reshape(pred_y, (batch_size, 3000,16)))
        
        x = jnp.ravel(x)
        y = jnp.ravel(y)
        loss_mse = (pred_y - (y-1))**2
        loss_mse_nozeros = jnp.where(y>0, loss_mse, 0)
        return loss_mse_nozeros.sum() / jnp.count_nonzero(loss_mse_nozeros)
    
    @eqx.filter_jit
    def make_step(model, x,y, opt_state):
        print('======')
        loss, grads = compute_loss(model, x, y)
        updates, opt_state = optim.update(grads, opt_state)
        model = eqx.apply_updates(model, updates)
        x = jnp.reshape(x, (x.shape[0], x.shape[1], x.shape[2], x.shape[3]))
        y = jnp.reshape(y, (y.shape[0], y.shape[1], y.shape[2]))
        return loss, model, opt_state
    
    optim = optax.adam(learning_rate)
    opt_state = optim.init(eqx.filter(model, eqx.is_array))
    loss_vector = jnp.empty(steps)

    for step, (x, y) in zip(range(steps), iter_data):
        
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size, 3000,16))
        loss, model, opt_state = make_step(model,x,y, opt_state)
        loss = loss.item()
        pred_y = jax.vmap(model)(x)
        pred_y = jnp.reshape(pred_y, (batch_size, 3000,16))
        loss_vector = loss_vector.at[step].set(loss)
        print(f"step={step}, loss={loss}")

    print('End Training :))')
    plt.plot(jnp.arange(steps), loss_vector, label='loss')
    plt.legend()
    plt.savefig('loss_eqx.png')

    xv = jnp.reshape(xv, (xv.shape[0], 1, 3000, 16))
    predicted_image = jax.vmap(model)(xv)
    predicted_image = jnp.reshape(predicted_image, (predicted_image.shape[0], 3000, 16))
    target_image = jnp.reshape(yv, (yv.shape[0], 3000, 16))

    predicted_image = predicted_image.flatten()
    target_image = target_image.flatten()

    print(predicted_image.shape, target_image.shape)

    predicted_image_signal = predicted_image[target_image==2]
    predicted_image_bkg = predicted_image[target_image==1]

    plt.figure(figsize=(8,6))
    plt.hist(predicted_image_signal, range=[-1,2.2], bins=200, alpha=0.5, label='signal')
    plt.hist(predicted_image_bkg, range=[-1,2.2], bins=200, alpha=0.5, label='bkg')
    plt.legend()
    plt.xlim([-0.2, 2.2])
    plt.yscale('log')
    plt.savefig('predictions_CNN_eqx.png')
    print('Finitoooo:))')

main()



